import torch
from torch.utils.data import DataLoader
from torchvision import datasets, models, transforms
from basiccnn import BasicCNN
import matplotlib.pyplot as plt
import torch.nn.functional as F
import numpy as np

class FashionMNIST:
    def __init__(self,cfg,batch_norm=False):
        self.batch_size = 64
        self.total_epochs = 100

        s = ''
        self.network_name = s.join('{}'.format(v) for v in cfg)
        if batch_norm:
            self.network_name += s.join('_BN')

        # Load the dataset
        self.work_dir = 'fashionmnist_output/'
        self.dataset_dir = '../dataset/fashionmnist'

        #print(test_set.test_data.float().mean()/255)
        #print(test_set.test_data.float().std()/255)

        # Augment the dataset
        trans = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.2868,), (0.3524,))])
        train_set = datasets.FashionMNIST(root=self.dataset_dir, train=True, transform=trans, download=True)
        test_set = datasets.FashionMNIST(root=self.dataset_dir, train=False, transform=trans, download=True)

        self.train_loader = torch.utils.data.DataLoader(
                 dataset=train_set,
                 batch_size=self.batch_size,
                 shuffle=True)
        self.test_loader = torch.utils.data.DataLoader(
                dataset=test_set,
                batch_size=self.batch_size,
                shuffle=False)

        # Define model
        self.model = BasicCNN(cfg,batch_norm).cuda()
        self.learning_rate = 0.001
        self.loss_fn = torch.nn.NLLLoss()
        #self.optimizer = torch.optim.Adagrad(self.model.parameters(),lr=self.learning_rate)
        #self.optimizer = torch.optim.SGD(self.model.parameters(), lr=self.learning_rate, momentum=0.9,weight_decay=1e-6)
        self.optimizer = torch.optim.Adam(self.model.parameters(),lr=self.learning_rate)

    def get_param_count(self):
        return np.sum(p.numel() for p in self.model.parameters() if p.requires_grad)

    def view(self):
        example = enumerate(self.test_loader)
        batch_idx,(image,label) = next(example)
        fig = plt.figure()
        last = []
        index = 0
        for i in range(64):
            if label[i] not in last:
                plt.subplot(3,4,index+1)
                plt.tight_layout()
                plt.imshow(image[i][0], cmap='gray', interpolation='none')
                plt.title('Ground Truth' + ": {}".format(label[i]))
                plt.xticks([])
                plt.yticks([])
                index = index+1
                if index == 10:
                    break
                last.append(label[i].item())
        fig.savefig('{}{}.{}'.format(self.work_dir,self.network_name,'png'))
        #plt.show()

    def plot_graph(self,train_x, train_y, test_x, test_y, ylabel='',name=''):
        fig = plt.figure()
        plt.plot(train_x, train_y, color='blue')
        plt.plot(test_x, test_y, color='red')
        plt.legend(['Train', 'Test'], loc='upper right')
        plt.xlabel('number of training examples seen')
        plt.ylabel(ylabel)
        plt.grid()
        #plt.show()
        fig.savefig('{}{}_{}.{}'.format(self.work_dir,self.network_name,name,'png'))

    def save_model(self):
        torch.save(self.model.state_dict(),'{}{}.pt'.format(self.work_dir,self.network_name))

    def save_predictions(self,path):
        self.model.eval()
        with torch.no_grad():
            for data,target in self.test_loader:
                data, target = data.cuda(), target.cuda()
                out = self.model(data)
                with open(path,"a") as out_file:
                    np.savetxt(out_file,out)


    def train_iteration(self,epoch,losses=[],counter=[],errors=[]):
        # Layer have different behaviour while training and evaluation
        # explicitly mentioning
        correct=0
        self.model.train()
        for batch_idx, (data,target) in enumerate(self.train_loader):
            self.optimizer.zero_grad()
            # Load dataset to the GPU
            data = data.cuda()
            target = target.cuda()
            out = self.model(data)
            loss = F.nll_loss(out,target)
            loss.backward()
            self.optimizer.step()
            if batch_idx % 64 == 0:
                print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                    epoch, batch_idx * len(data), len(self.train_loader.dataset),
                    100. * batch_idx / len(self.train_loader), loss.item()))
                losses.append(loss.item())
                counter_val = (batch_idx*self.batch_size) + ((epoch-1)*len(self.train_loader.dataset))
                counter.append(counter_val)

            pred = out.max(1, keepdim=True)[1]
            correct += pred.eq(target.view_as(pred)).sum().item()

        errors.append(100. * (1 - correct / len(self.train_loader.dataset)))

    def test(self,losses=[],errors=[]):
        self.model.eval()
        test_loss = 0
        correct = 0
        with torch.no_grad():
            for data, target in self.test_loader:
                data, target = data.cuda(), target.cuda()
                out = self.model(data)
                # sum up batch loss
                test_loss += F.nll_loss(out,target,reduction='sum').item()
                pred = out.max(1, keepdim=True)[1] # get the index of the max log-probability
                correct += pred.eq(target.view_as(pred)).sum().item()

        test_loss /= len(self.test_loader.dataset)
        print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
            test_loss, correct, len(self.test_loader.dataset),
            100. * correct / len(self.test_loader.dataset)))

        path = '{}{}_accuracies.txt'.format(self.work_dir,self.network_name)
        with open(path,"a") as out_file:
            out_file.write('{}\r\n'.format(100. * correct / len(self.test_loader.dataset)))

        losses.append(test_loss)
        errors.append(100. *  (1 - correct / len(self.test_loader.dataset)))

    def train(self):
        train_losses = []
        train_errors = []
        train_counter = []

        test_losses = []
        test_errors = []
        test_counter = [i*len(self.train_loader.dataset) for i in range(self.total_epochs + 1)]

        error_counter = [i*len(self.train_loader.dataset) for i in range(self.total_epochs)]

        self.test(losses=test_losses)

        for epoch in range(1,self.total_epochs + 1):
            self.train_iteration(epoch,train_losses,train_counter,train_errors)
            self.test(test_losses,test_errors)
        #Save save predictions
        self.save_predictions('{}{}_pred.txt'.format(self.work_dir,self.network_name))
        #Save model
        self.save_model()
        # Show graphs
        self.plot_graph(train_counter, train_losses, test_counter, test_losses, ylabel='negative log likelihood loss',name='losses')
        self.plot_graph(error_counter, train_errors, error_counter, test_errors, ylabel='error (%)',name='errors')
