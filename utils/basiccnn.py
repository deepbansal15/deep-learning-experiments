import torch
import torch.nn as nn
import torch.nn.functional as F

class BasicCNN(torch.nn.Module):
    def __init__(self,cfg,batch_norm=False):
        super(BasicCNN, self).__init__()
        self.conv_layers = self.make_layers(cfg,batch_norm)

        self.dropout1 = torch.nn.Dropout(p=0.2)

        self.fcn1 = torch.nn.Linear(in_features=128 * 3 * 3,out_features=2048)
        self.relu1 = torch.nn.ReLU()
        self.dropout2 = torch.nn.Dropout(p=0.2)

        # self.fcn2 = torch.nn.Linear(in_features=2048,out_features=1000)
        # self.relu2 = torch.nn.ReLU()
        # self.dropout3 = torch.nn.Dropout(p=0.2)

        self.fcn3 = torch.nn.Linear(in_features=2048,out_features=10)

    def make_layers(self,cfg,batch_norm=False):
        layers = []
        input_c = 1
        for k in cfg:
            if k == 'm':
                layers += [torch.nn.MaxPool2d(kernel_size=2)]
            else:
                conv2d = torch.nn.Conv2d(in_channels=input_c,out_channels=k,kernel_size=3,padding=1)
                if batch_norm:
                    layers += [conv2d,torch.nn.BatchNorm2d(k),torch.nn.ReLU()]
                else:
                    layers += [conv2d,torch.nn.ReLU()]

                input_c = k

        return torch.nn.Sequential(*layers)

    def forward(self,x):
        x = self.conv_layers(x)
        x = x.view(-1,128 * 3 * 3)

        x = self.dropout1(x)

        x = self.fcn1(x)
        x = self.relu1(x)

        x = self.dropout2(x)

        # x = self.fcn2(x)
        # x = self.relu2(x)

        # x = self.dropout3(x)

        x = self.fcn3(x)

        return F.log_softmax(x, dim=1)
