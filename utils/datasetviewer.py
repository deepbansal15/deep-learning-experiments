import torch
from torch.utils.data import DataLoader
from torchvision import datasets, models, transforms
import matplotlib.pyplot as plt

def main():
    dataset_dir = '../dataset/fashionmnist'
    trans = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5,), (0.5,))])
    train_set = datasets.FashionMNIST(root=dataset_dir, train=True, transform=trans, download=True)
    test_set = datasets.FashionMNIST(root=dataset_dir, train=False, transform=trans, download=True)

    train_loader = torch.utils.data.DataLoader(
        dataset=train_set,
        batch_size=64,
        shuffle=True)
    test_loader = torch.utils.data.DataLoader(
            dataset=test_set,
            batch_size=64,
            shuffle=False)

    example = enumerate(test_loader)
    batch_idx,(image,label) = next(example)
    print(label)

    fig = plt.figure()
    last = []
    index = 0
    for i in range(64):
        if label[i] not in last:
            plt.subplot(3,4,index+1)
            plt.tight_layout()
            plt.imshow(image[i][0], cmap='gray', interpolation='none')
            plt.title('Ground Truth' + ": {}".format(label[i]))
            plt.xticks([])
            plt.yticks([])
            index = index+1
            if index == 10:
                break
            last.append(label[i].item())
    plt.show()

if __name__== "__main__":
    main()
