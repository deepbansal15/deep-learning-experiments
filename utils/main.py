
import numpy as np
import torch
from torchvision import datasets, models, transforms
from fashionmnist import FashionMNIST

def main():
    cfg = {
        '1':[32,'m',64,'m',128,'m'],
        '2':[32,32,'m',64,'m',128,'m'],
        '3':[32,32,'m',64,64,'m',128,'m'],
        '4':[32,32,'m',64,64,'m',128,128,'m']
    }

    # for i in cfg:
        # mnist = FashionMNIST(cfg[i])
        # print(mnist.get_param_count())
        # mnist.train()
    mnist = FashionMNIST(cfg['4'],batch_norm=True)
    mnist.train()


def pytorch_nn():

    # Load datasets

    train_dataset = datasets.FashionMNIST('../dataset/fashionmnist',train=True,download=True)
    train_loader = torch.utils.data.DataLoader(train_dataset)
    test_dataset = datasets.FashionMNIST('../dataset/fashionmnist',train=False,download=True)
    test_loader = torch.utils.data.DataLoader(train_dataset)
    # N is batch size; D_in is input dimension;
    # H is hidden dimension; D_out is output dimension.
    N, D_in, H, D_out = 64, 1000, 100, 10

    # Create random Tensors to hold inputs and outputs
    x = torch.randn(N, D_in)
    y = torch.randn(N, D_out)

    # Use the nn package to define our model as a sequence of layers. nn.Sequential
    # is a Module which contains other Modules, and applies them in sequence to
    # produce its output. Each Linear Module computes output from input using a
    # linear function, and holds internal Tensors for its weight and bias.
    model = torch.nn.Sequential(
        torch.nn.Linear(D_in, H),
        torch.nn.ReLU(),
        torch.nn.Linear(H, D_out),
    )

    loss_fn = torch.nn.MSELoss(reduction='sum')
    learning_rate = 1e-4
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
    for t in range(500):
        y_pred = model(x)
        loss = loss_fn(y_pred,y)
        print(t,loss.item())

        model.zero_grad()
        loss.backward()
        optimizer.step()


def tensor_exp():
    dtype = torch.float
    device = torch.device("cpu")
    # device = torch.device("cuda:0") # Uncomment this to run on GPU

    # N is batch size; D_in is input dimension;
    # H is hidden dimension; D_out is output dimension.
    N, D_in, H, D_out = 64, 1000, 100, 10

    # Create random input and output data
    x = torch.randn(N, D_in, device=device, dtype=dtype)
    y = torch.randn(N, D_out, device=device, dtype=dtype)

    # Randomly initialize weights
    w1 = torch.randn(D_in, H, device=device, dtype=dtype)
    w2 = torch.randn(H, D_out, device=device, dtype=dtype)

    learning_rate = 1e-6
    for t in range(500):
        # Forward pass: compute predicted y
        h = x.mm(w1)
        h_relu = h.clamp(min=0)
        y_pred = h_relu.mm(w2)

        # Compute and print loss
        loss = (y_pred - y).pow(2).sum().item()
        print(t, loss)

        # Backprop to compute gradients of w1 and w2 with respect to loss
        grad_y_pred = 2.0 * (y_pred - y)
        grad_w2 = h_relu.t().mm(grad_y_pred)
        grad_h_relu = grad_y_pred.mm(w2.t())
        grad_h = grad_h_relu.clone()
        grad_h[h < 0] = 0
        grad_w1 = x.t().mm(grad_h)

        # Update weights using gradient descent
        w1 -= learning_rate * grad_w1
        w2 -= learning_rate * grad_w2

def numpy_exp():
    N,D_in,H,D_out = 64,1000,100,10
    x = np.random.randn(N,D_in)
    y = np.random.randn(N,D_out)

    w1 = np.random.randn(D_in,H)
    w2 = np.random.randn(H,D_out)

    learning_rate = 1e-6

    for t in range(500):
        h = x.dot(w1)
        h_relu = np.maximum(h,0)
        y_pred = h_relu.dot(w2)

        loss = np.square(y_pred-y).sum()
        print(t,loss)
        grad_y_pred = 2.0 * (y_pred-y)
        grad_w2 = h_relu.T.dot(grad_y_pred)
        grad_h_relu = grad_y_pred.dot(w2.T)
        grad_h = grad_h_relu.copy()
        grad_h[h<0] = 0
        grad_w1 = x.T.dot(grad_h)

        w1 -= learning_rate * grad_w1
        w2 -= learning_rate * grad_w2

    print(w1)
    print(w2)

if __name__== "__main__":
    main()
    # pytorch_nn()
    # numpy_exp()
    # tensor_exp()
